**Manual for Age detection (AGE) service**

**HENSOLDT ANALYTICS Technology**

HENSOLDT ANALYTICS MediaMiningIndexer Age detection provides a speaker age information that can be detected from audio recording. Input is an audio recording and output is age-bracket. Currently supported are three result brackets:
  * child
  * adult
  * unknown

With the bracket there is also a score of the bracked assignment in range from 0.0 to 1.0 where 1.0 is better.

**Available ELG endpoints**

hens-age

**Input**

Input is audio file supported by ELG platform.

```
curl -k -H "Authorization: Bearer $TOKEN" -H 'Content-Type:audio/x-wav' --data '@./path/auido.wav' https://live.european-language-grid.eu/execution/async/processAudio/hens-age
```

**Output**

ELG Json response format, with annotations format and single feature that specifies age of the speaker and score.

```
{ 
  "response" : { 
    "type" : "annotations",
    "annotations" : null,
    "features" : 
    { 
      "age" : "adult", 
      "score" : "0.94"
    }
  }
}
```
