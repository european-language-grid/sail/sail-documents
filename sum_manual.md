**Manual for Summarization (SUM) service**

**HENSOLDT ANALYTICS Technology**

HENSOLDT ANALYTICS Summarization service takes a text input and returns a shorter excerpt from the text that covers the main topics of the text.

**Available ELG endpoints**

hens-sum

**Input**

Text or text file, one line or multiple lines which will be treated as paragraphs.

Input is utf-8 text file.

```
curl -k -H "Authorization: Bearer $TOKEN" -H 'Content-Type:text/plain' --data '@./path/london-weather.txt' https://live.european-language-grid.eu/execution/async/processText/hens-sum
```

curl parameter with data can be either --data "text enclosed in quotes" or it can be also --data-binary @/file/path

**Output**

ELG Json response format, with texts response with single text object. Text role is "summary" and content holds the selected text.

```
{
  "response" : {
    "type" : "texts",
    "texts" : [ 
      {
        "role" : "summary",
        "content" : "London has a temperate oceanic climate."
      }
    ]
  }
}
```
