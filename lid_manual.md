**Manual for Languge Identification (LID) service**

**HENSOLDT ANALYTICS Technology**

This service takes a text (file) input and returns zero or more language identifiers with confidence rating for each of them, sorted from most confident one.
Supported languages are: 
Arabic, Bulgarian, Catalan, Czech, Danish, 
German, Greek, English, Spanish, Farsi,
French, Hebrew, Croatian, Hungarian, Bahasa Indonesia,
Italian, Malay, Dutch, Norwegian, Polish,
Pashto, Portugese, Romanian, Russian, Slovak,
Tosk Albanian, Swedish, Swaheli, Turkish, Urdu, 
Chinese Simplified.

**Available ELG endpoints**

hens-lid

**Input**

curl -k -H "Authorization: Bearer $TOKEN" -H 'Content-Type: text/plain' --data '@./path/somefile' https://live.european-language-grid.eu/execution/async/processText/hens-lid

where parameter to --data can be either @ with file path, or text enclosed in quotes

**Output**

json in format:

```
 { "response" : { "type" : "classification" ,
   "classes" : [
      {"class":"en","score":0.78 },
      {"class":"cs","score":0.45 },
   ]
 }}
```
