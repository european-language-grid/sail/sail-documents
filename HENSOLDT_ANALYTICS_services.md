**HENSOLDT ANALYTICS Technology**

HENSOLDT ANALYTICS services for [Speech to text](asr_manual.md), [Language identification](lid_manual.md), [Sentiment analysis](sed_manual.md) and [Named entities detection](ned_manual.md), [Keyword spotting](kws_manual.md), [Age detection](age_manual.md), [Gender detection](gen_manual.md), [Summarization](sum_manual.md).

For information about services please first read their manuals, and for additional information contact us at Miroslav.Janosik@hensoldt-analytics.com

For information about license please read [HENSOLDT ANALYTICS ELG License](https://gitlab.com/european-language-grid/sail/sail-documents/-/blob/master/HENSOLDT_ANALYTICS_services.md) or contact us at info@hensoldt-analytics.com .

**Example usage**

Lets try to combine some of the scripts, in a form of bash script:

```
#!/bin/bash

# show what is going on
set -x

# my credentials
USR=johndoe
PWD=spain
INPUT=data.txt

# lets get the access token for my user
TOKEN=$(curl --location --request POST 'https://live.european-language-grid.eu/auth/realms/ELG/protocol/openid-connect/token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'client_id=react-client' \
--data-urlencode 'grant_type=password' \
--data-urlencode "username=$USR" \
--data-urlencode "password=$PWD" | jq -r .access_token)

# jq is a tool that extracts only the 'access_token' part from the json response; you may need to install that

# We have some unknown text file here. First lets see what is the language of the text.
TEXTSERVICEURL=https://live.european-language-grid.eu/execution/processText

curl -H "Authorization: Bearer $TOKEN" -H 'Content-Type: text/plain' --data-binary "@$INPUT" $TEXTSERVICEURL/hens-lid > langid.out

# Response is in this form:
# { "response" : { "type" : "classification" ,  "classes" : [  {"class":"en","score":0.78 },  {"class":"cs","score":0.35 }, ]  }}
# So lets use jq again to get only first (most probable) language:
LANG=$(cat langid.out | jq -r .response.classes[0].class)

# Now, lets use the language to get sentiment of the text and interesting words
curl -H "Authorization: Bearer $TOKEN" -H 'Content-Type: text/plain' --data-binary "@$INPUT" $TEXTSERVICEURL/hens-sed-$LANG > sentiment.out

curl -H "Authorization: Bearer $TOKEN" -H 'Content-Type: text/plain' --data-binary "@$INPUT" $TEXTSERVICEURL/hens-ned-$LANG > named-entities.out
cat named-entities.out | jq -r .response.texts[].annotations > named-entities.txt

# And there we have it! That is sentiment and interesting words in the text, with automatically determining the services according the text language!
```
