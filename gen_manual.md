**Manual for Gender detection (GEN) service**

**HENSOLDT ANALYTICS Technology**

HENSOLDT ANALYTICS MediaMiningIndexer Gender detection provides a speaker gender information that can be detected from audio recording. Input is an audio recording and output is gender-bracket. Currently supported are three result brackets:
  * female
  * male
  * unknown

With the bracket there is also a score of the bracked assignment in range from 0.0 to 1.0 where 1.0 is better.

**Available ELG endpoints**

hens-gen

**Input**

Input is audio file supported by ELG platform.

```
curl -k -H "Authorization: Bearer $TOKEN" -H 'Content-Type:audio/x-wav' --data '@./path/auido.wav' https://live.european-language-grid.eu/execution/async/processAudio/hens-gen
```

**Output**

ELG Json response format, with annotations format and single feature that specifies gender of the speaker and score.

```
{ 
  "response" : { 
    "type" : "annotations",
    "annotations" : null,
    "features" : 
      { 
        "gender" : "female", 
        "score" : "0.94"
      }
  }
}
```
