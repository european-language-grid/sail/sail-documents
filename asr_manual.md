**Manual for Automated Speech Recognition (ASR) service**

**HENSOLDT ANALYTICS Technology**

This service takes an audio (wav/mp3) input and returns segments of text with individual words and their timing.

**Available endpoints**

One for each language: English, German, French, Spanish, Greek, Czech, Norwegian, Dutch, Polish, Romanian, Albanian, Turkish, Italian

Example url:
> https://live.european-language-grid.eu/execution/async/processAudio/hens-asr-en 

Asking for response synchronously through sync request is not encouraged, as processing takes about 1 second for 1 second of audio so a timeout may occur on audio longer than 15 seconds.

**Input**

Audio file in mp3 (mimetype audio/mpeg) or wav (mimetype audio/x-wav) format, sent with curl parameter --data-binary @/file/path

**Example calls**

Call with parameter as file
> curl -H "Authorization: Bearer $TOKEN" -H "Content-Type: audio/mpeg" --data-binary @/path/to/file.mp3 https://live.european-language-grid.eu/execution/async/processAudio/hens-asr-en

**Output**

Returns "texts" format with segments of text. Each segment contains the segment text in "content" and individual words in annotations features. 
Variables *start* and *end* is position of word in the *content* text, *sourceStart* and *sourceEnd* is time of the word in source audio in seconds:

```
{ "response": {
    "type": "texts",
    "texts": [
        {
            "role": "segment",
            "content": "Hello everyone.",
            "annotations": {
                "entity": [
                    {
                        "start": 0, "end": 5,
                        "sourceStart": 0, "sourceEnd": 0.41,
                        "features": { "text": "Hello" }
                    },
                    {
                        "start": 6, "end": 14,
                        "sourceStart": 0.41, "sourceEnd": 0.79,
                        "features": { "text": "everyone" }
                    },
                ]
            }
        },
        {
            "role": "segment",
            "content": "Banana apple.",
            "annotations": {
                "entity": [
                    {
                        "start": 0, "end": 5,
                        "sourceStart": 3.00, "sourceEnd": 3.65,
                        "features": { "text": "Banana" }
                    },
                    {
                        "start": 6, "end": 12,
                        "sourceStart": 3.85, "sourceEnd": 4.25,
                        "features": { "text": "apple" }
                    },
                ]
            }
        }
    ]
}}
```
