**Manual for Sentiment Analysis (SED) service**

**HENSOLDT ANALYTICS Technology**

This service takes a text (file) input and returns sentiment information on the text or its segments.

**Available endpoints**

One for each language: English, German, French, Spanish
One for each language, "hens-sed-<language code>" where <language code> is one of: English (en), German (de), French (fr), Spanish (es), Italian (it), Polish (pl), Portugese (pt).

Example url:
> https://live.european-language-grid.eu/execution/async/processText/hens-sed-en 

You can also ask for response synchronously through sync request:
> https://live.european-language-grid.eu/execution/processText/hens-sed-en

**Input**

Text or text file, one line or multiple lines which will be treated as paragraphs.

curl parameter with data can be either --data "text enclosed in quotes" or it can be --data-binary @/file/path

**Example calls**

Call with parameter as file, specifying output type
> curl -H "Authorization: Bearer $TOKEN" -H "Content-Type: text/plain" --data-binary @/path/to/file.txt https://live.european-language-grid.eu/execution/processText/hens-sed-en

**Output**

Default returns "annotations" format with sentiment for overall text, type output=annotations:

```
 { "response" : {"type" : "annotations", 
   "annotations": null, 
   "features": {
      "polarity": "NEGATIVE", 
      "positiveSentiment": "0.5", 
      "negativeSentiment": "-1.2"
   }
 }}
 
```

Output type output=texts with sentiment separated for each paragraph, and one overall sentiment:
```
 { "response" : { 
    "type": "texts", 
    "texts": [
    {
        "role": "paragraph", 
        "content": "Did Nikola Tesla live in Berlin?. ", 
        "features": {
            "polarity": "NEUTRAL", 
            "positiveSentiment": "0", 
            "negativeSentiment": "0"
        }
    }, 
    {
        "role": "paragraph", 
        "content": "Or did he live in London?. ", 
        "features": {
            "polarity": "NEUTRAL", 
            "positiveSentiment": "0", 
            "negativeSentiment": "0"
        }
    }], 
    "features": {
        "polarity": "NEUTRAL", 
        "positiveSentiment": "0", 
        "negativeSentiment": "0"
    }
 }}
```
