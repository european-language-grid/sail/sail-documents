**Manual for Keyword spotting (KWS) service**

**HENSOLDT ANALYTICS Technology**

HENSOLDT ANALYTICS Keywords spotting service takes an audio (wav/mp3) input with list of keywords and returns annotations of the audio input with possible occurences of input keywords with time at which they occur.

**Available ELG endpoints**

One for each language.

Example url:
> https://live.european-language-grid.eu/execution/async/processAudio/hens-kws-en 

Asking for response synchronously through sync request is not encouraged, as processing takes about 1 second for 1 second of audio so a timeout may occur on audio longer than 15 seconds.

**Input**

Audio file in mp3 (mimetype audio/mpeg) or wav (mimetype audio/x-wav) format, sent with curl parameter --data-binary @/file/path ; additionally to that a keyword parameter has to be sent.

```
curl -H "Authorization: Bearer $TOKEN" -H "Content-Type: audio/x-wav" --data-binary "@./audio.wav" "https://live.european-language-grid.eu/execution/async/processAudio/hens-kws-en?keywords%5B%5D=first&keywords%5B%5D=second"

```

Optionally keywords can be sent as file payload with content-type "application/json" in this form:

> { "params" : { "keywords" : [ "first", "second" ] } }

**Output**

ELG Json response format, with annotations response. Keyword are in the annotations structure and for each found occurence it contains audio start and end time in seconds.

```
{
  "response": {
    "annotations": {
      "first": [
        {
          "end": 8.95,
          "start": 8.87
        },
        {
          "end": 14.49,
          "start": 14.5
        },
        {
          "end": 22.11,
          "start": 21.98
        },
        {
          "end": 303.62,
          "start": 303.18
        }
      ],
      "second": []
    },
    "features": {
      "found": "4",
      "info": ""
    },
    "type": "annotations"
  }
}```

