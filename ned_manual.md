**Manual for Named Entities (NED) service**

**HENSOLDT ANALYTICS Technology**

This service takes a text (file) input and returns annotations of the words with their class.

**Available endpoints**

One for each language, "hens-ned-<language-code>" where <language-code> is one of: English (en), German (de), French (fr), Spanish (es), Greek (el), Czech (cz), Bulgarian (bg), Catalan (ca), Croatian (hr), Hungarian (hr), Italian (it), Dutch (nl), Norweigan (no), Polish (pl), Portugese (pt), Romaian (ro), Slovak (sk), Albanese (sq), Swedish (sv), Turkish (tr).

Example url:
> https://live.european-language-grid.eu/execution/async/processText/hens-ned-en 

You can also ask for response synchronously through sync request:
> https://live.european-language-grid.eu/execution/processText/hens-ned-en

**Input**

Text or text file, one line or multiple lines which will be treated as paragraphs.

curl parameter with data can be either --data "text enclosed in quotes" or it can be --data-binary @/file/path

**Example calls**

Call with simple text request:
> curl -H "Authorization: Bearer $TOKEN" -H "Content-Type: text/plain" --data 'Did Nikola Tesla live in Berlin?' https://live.european-language-grid.eu/execution/processText/hens-ned-en 

Call with multi-line request and specifying output type
> curl -X POST -H "Authorization: Bearer $TOKEN" -H "Content-Type:application/json" -d "{ \"params\":{\"output\":\"annotations\"}, \"type\":\"text\",\"content\":\"Did Nikola Tesla live in Berlin?\nOr did he live in London?\",\"mimeType\":\"text/plain\"}" https://live.european-language-grid.eu/execution/processText/hens-ned-en

Call with parameter as file, specifying output type
> curl -H "Authorization: Bearer $TOKEN" -H "Content-Type: text/plain" --data-binary @/path/to/file.txt https://live.european-language-grid.eu/execution/processText/hens-ned-en?output=texts

**Output**

Returns annotation features of these types: person, organization, location

For this two-line input:
```
Did Nikola Tesla live in Berlin?
Or did he live in London?
```

Default returns "texts" format with all paragraphs merged into single text, type output=textsMerged:
```
{ "response": {
    "type": "texts"
    "texts": [
        {
            "content": "Did Nikola Tesla live in Berlin? .  Or did he live in London? . ", 
            "role": "paragraph", 
            "annotations": {
                "person": [{"start": "4", "end": "16", "features": {"text": "Nikola Tesla"}}], 
                "location": [{"start": "25", "end": "32", "features": {"text": "Berlin?"}}, {"start": "53", "end": "60", "features": {"text": "London?"}}]
            }
        }
    ]}
}
```

Output type output=texts each paragraph separated:

```
{ "response": {
        "type": "texts"
        "texts": [
            {
                "role": "paragraph",
                "content": "Did Nikola Tesla live in Berlin? . ",
                "annotations": {
                    "person": [{ "start":"4", "end":"16", "features":{ "text":"Nikola Tesla" }}],
                    "location": [{ "start":"25", "end":"32", "features":{ "text":"Berlin?" }}]
                }
            },
            {
                "role": "paragraph",
                "content": "Or did he live in London? . ",
                "annotations": {
                    "location": [{ "start": "18", "end": "25", "features": { "text": "London?" }}]
                }
            }
        ],
    }
}
```

Output type output=annotations contains only entities for all paragraphs merged:

```
{ "response": {
    "type": "annotations",
    "annotations": {
        "person": [{ "start": "4", "end": "16", "features": { "text": "Nikola Tesla" }}],
        "location": [{ "start": "25", "end": "32", "features": { "text": "Berlin?" }}, { "start": "54", "end": "61", "features": { "text": "London?" }}]
        }
    }
}
```
